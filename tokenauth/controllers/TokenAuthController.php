<?php
namespace Craft;

/**
 * TokenAuthController
 *
 * @author    Top Shelf Craft <michael@michaelrog.com>
 * @copyright Copyright (c) 2016, Michael Rog
 * @license   http://topshelfcraft.com/license
 * @see       http://topshelfcraft.com
 * @package   craft.plugins.tokenauth
 * @since     1.0
 */
class TokenAuthController extends BaseController
{


    protected $allowAnonymous = true;


	/**
	 *
	 */
    public function actionAuthenticateUser()
    {

        $this->requirePostRequest();

	    // This method mimics the logic found in UserSessionService->login()
	    // We can't simply pass through to that method, however, because it sets a user cookie by default, which we don't want.

        $username = craft()->request->getPost('login_name');
        $password = craft()->request->getPost('password');

	    $usernameModel = new UsernameModel();
	    $passwordModel = new PasswordModel();

	    $usernameModel->username = $username;
	    $passwordModel->password = $password;

	    // Validate the username/password models.

	    if ($usernameModel->validate() && $passwordModel->validate())
	    {

	    	// If the components are valid, spin up a new ID

		    $userIdentity = new UserIdentity($username, $password);

		    // Try to authenticate the ID

		    if ($userIdentity->authenticate())
		    {

		    	// Return the user
		    	$user = $userIdentity->getUserModel();
		    	$token = craft()->tokenAuth->getTokenForUser($user->id, true);

			    if (!empty($token))
			    {
			    	$this->returnJson(array(
					    'success' => true,
			    		'user_id' => $user->id,
			    		'email' => $user->email,
					    'first_name' => $user->firstName,
					    'last_name' => $user->lastName,
					    'full_name' => $user->getName(),
					    'token' => $token->token,
					    'token_expiry' => $token->expiryDate->w3c(),
				    ));
			    }
			    else
			    {
				    $this->returnErrorJson("The user could not be authenticated.");
				    TokenAuthPlugin::log($username.' authenticated successully, but there was a problem creating the token.', LogLevel::Error);
			    }

		    }
		    else
		    {
			    $this->returnErrorJson("The user could not be authenticated.");
			    TokenAuthPlugin::log($username.' tried to authenticate unsuccessfully.', LogLevel::Warning);
		    }

	    }
	    else
	    {
		    $this->returnErrorJson("Username and/or password is not valid.");
	    }

    }


	/**
	 *
	 */
    public function actionExtendToken()
    {

    	// Validate the request

	    $this->requirePostRequest();

	    // Make sure we authenticate a user

	    craft()->tokenAuth->authenticateCurrentRequestToken();
	    $activeToken = craft()->tokenAuth->getActiveToken();
	    if (!($activeToken instanceof TokenAuth_UserTokenModel))
	    {
		    $this->returnErrorJson("The user could not be authenticated.");
	    }

	    // Extend the token if allowed

	    if (craft()->config->get('userCanExtendOwnToken', 'tokenauth'))
	    {

	    	$success = craft()->tokenAuth->extendToken($activeToken);

		    if ($success)
		    {

			    $updatedToken = craft()->tokenAuth->getTokenByString($activeToken->token);

				$this->returnJson([
		    		'success' => true,
				    'token' => $updatedToken->token,
					'token_expiry' => $updatedToken->expiryDate->w3c(),
			    ]);

		    }
		    else
		    {
			    $this->returnErrorJson("Problem extending the token.");
			    TokenAuthPlugin::log("Problem extending token {$activeToken->token}.", LogLevel::Error);
		    }

	    }
	    else
	    {
		    $this->returnErrorJson("User is not allowed to extend own token.");
		    TokenAuthPlugin::log("User {$activeToken->userId} is not allowed to extend token {$activeToken->token}.", LogLevel::Warning);
	    }

    }


	/**
	 *
	 */
	public function actionExpireToken()
	{

		// Validate the request

		$this->requirePostRequest();

		// Make sure we authenticate a user

		craft()->tokenAuth->authenticateCurrentRequestToken();
		$activeToken = craft()->tokenAuth->getActiveToken();
		if (!($activeToken instanceof TokenAuth_UserTokenModel))
		{
			$this->returnErrorJson("The user could not be authenticated.");
		}

		// Expire the token if allowed

		if (craft()->config->get('userCanExpireOwnToken', 'tokenauth'))
		{

			$success = craft()->tokenAuth->expireToken($activeToken);

			if ($success)
			{
				$this->returnJson([
					'success' => true,
				]);
			}
			else
			{
				$this->returnErrorJson("Problem expiring the token.");
				TokenAuthPlugin::log("Problem expiring token {$activeToken->token}.", LogLevel::Error);
			}

		}
		else
		{
			$this->returnErrorJson("User is not allowed to expire own token.");
			TokenAuthPlugin::log("User {$activeToken->userId} is not allowed to expire token {$activeToken->token}.", LogLevel::Warning);
		}

	}


}

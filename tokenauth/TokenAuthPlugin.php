<?php
namespace Craft;

/**
 * TokenAuthPlugin
 *
 * @author    Top Shelf Craft <michael@michaelrog.com>
 * @copyright Copyright (c) 2016, Michael Rog
 * @license   http://topshelfcraft.com/license
 * @see       http://topshelfcraft.com
 * @package   craft.plugins.tokenauth
 * @since     1.0
 */
class TokenAuthPlugin extends BasePlugin
{


	/**
	 * Return the plugin name
	 *
	 * @return string
	 */
	public function getName()
	{
		return 'Token Authentication';
	}

	/**
	 * Return the plugin developer's name
	 *
	 * @return string
	 */
	public function getDeveloper()
	{
		return 'Top Shelf Craft';
	}

	/**
	 * Return the plugin developer's URL
	 *
	 * @return string
	 */
	public function getDeveloperUrl()
	{
		return 'https://topshelfcraft.com';
	}

	/**
	 * Return the plugin's Documentation URL
	 *
	 * @return string
	 */
	public function getDocumentationUrl()
	{
		return '';
	}

	/**
	 * Return the plugin's Release Feed URL
	 *
	 * @return string
	 */
	public function getReleaseFeedUrl()
	{
		return '';
	}

	/**
	 * Return the current version
	 *
	 * @return string
	 */
	public function getVersion()
	{
		return '0.0.0';
	}

	/**
	 * Return the database schema version
	 *
	 * @return string
	 */
	public function getSchemaVersion()
	{
		return '0.0.0.0';
	}

	/**
	 * Return whether or not the plugin has a CP section
	 *
	 * @return bool
	 */
	public function hasCpSection()
	{
		return false;
	}

	/**
	 * Make sure requirements are met before installation.
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function onBeforeInstall()
	{

		// Prevent the install if we aren't at least on Craft 2.5

		if (version_compare(craft()->getVersion(), '2.5', '<')) {
			// No way to gracefully handle this
			// (because until 2.5, plugins can't prevent themselves from being installed),
			// so throw an Exception.
			throw new Exception('The Token Authentication plugin requires Craft 2.5+');
		}

		// Prevent the install if we aren't at least on PHP 5.4

		if (!defined('PHP_VERSION_ID') || PHP_VERSION_ID < 50400) {
			Craft::log('The Token Authentication plugin requires PHP 5.4+', LogLevel::Error);
			return false;
			// TODO: Flash an error to the CP before returning.
		}

	}

	/**
	 * Init the plugin
	 */
	public function init() {

		// TODO

	}

	/**
	 * Logs a message or value to the plugin log file
	 *
	 * @param string $msg
	 * @param string $level
	 * @param bool $force
	 *
	 * @return void
	 */
	public static function log($msg, $level = LogLevel::Profile, $force = false)
	{

		if (is_string($msg))
		{
			$msg = "\n" . $msg . "\n\n";
		}
		else
		{
			$msg = "\n" . print_r($msg, true) . "\n\n";
		}

		parent::log($msg, $level, $force);

	}


}

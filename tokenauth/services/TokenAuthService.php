<?php
namespace Craft;

/**
 * TokenAuthService
 *
 * @author    Top Shelf Craft <michael@michaelrog.com>
 * @copyright Copyright (c) 2016, Michael Rog
 * @license   http://topshelfcraft.com/license
 * @see       http://topshelfcraft.com
 * @package   craft.plugins.tokenauth
 * @since     1.0
 */
class TokenAuthService extends BaseApplicationComponent
{


	private $_activeToken = null;

	private $_defaultTokenDuration;


	/**
	 *
	 */
	public function init()
	{

		parent::init();

		if (! empty(craft()->config->get('defaultTokenDuration', 'tokenauth')))
		{
			$this->_defaultTokenDuration = craft()->config->get('defaultTokenDuration', 'tokenauth');
		}
		else
		{
			$this->_defaultTokenDuration = craft()->config->get('defaultTokenDuration');
		}

	}


	/**
	 * Creates a new token for the user
	 *
	 * @param UserModel|int $user
	 * @param DateTime|null $expiryDate
	 *
	 * @return TokenAuth_UserTokenRecord|null
	 */
    public function createTokenForUser($user = null, $expiryDate = null)
    {

    	// We need a valid User Id

    	if (empty($user))
	    {
	    	return null;
	    }
	    elseif($user instanceof UserModel)
	    {
	    	$user = $user->id;
	    }
	    elseif(!is_numeric($user))
	    {
	    	return null;
	    }

	    // Set a default-length expiryDate if a specific one isn't provided

        if (empty($expiryDate))
        {
            $expiryDate = DateTimeHelper::currentUTCDateTime();
            $expiryDate->add(new DateInterval($this->_defaultTokenDuration));
        }

        // Create and return the token

        $authTokenRecord = new TokenAuth_UserTokenRecord();
	    $authTokenRecord->userId = $user;
	    $authTokenRecord->token = StringHelper::UUID();
        $authTokenRecord->expiryDate = $expiryDate;

	    $success = $authTokenRecord->save();

        if ($success)
        {
            return TokenAuth_UserTokenModel::populateModel($authTokenRecord);
        }
        else
        {
            return null;
        }

    }


	/**
	 * Gets a valid token for the user.
	 * If the user has multiple valid tokens, this method returns the one with the furthest-future expiry date.
	 * If the user has no tokens, but $createIfNoneFound is enabled, the method will try to create a new one.
	 *
	 * @param UserModel|int $user
	 * @param bool $createIfNoneFound
	 *
	 * @return TokenAuth_UserTokenRecord|null
	 */
    public function getTokenForUser($user = null, $createIfNoneFound = false)
    {

	    // We need a valid User Id

	    if (empty($user))
	    {
		    return null;
	    }
	    elseif($user instanceof UserModel)
	    {
		    $user = $user->id;
	    }
	    elseif(!is_numeric($user))
	    {
		    return null;
	    }

	    // Find a token if possible.

	    $tokenRecord = TokenAuth_UserTokenRecord::model()->notExpired()->longestValid()->findByAttributes([
		    'userId' => $user
	    ]);

	    // Return the token if found, or if createIfNoneFound is enabled, return a newly created token.

	    if (!empty($tokenRecord))
	    {
	    	return TokenAuth_UserTokenModel::populateModel($tokenRecord);
	    }
	    elseif ($createIfNoneFound)
	    {
	    	return $this->createTokenForUser($user);
	    }
	    else
	    {
	    	return null;
	    }

    }


	/**
	 * Returns the user token that matches the provided token string.
	 * Optionally includes expired tokens.
	 *
	 * @param string $token
	 * @param bool $includeExpired
	 *
	 * @return TokenAuth_UserTokenModel|null
	 */
    public function getTokenByString($token = null, $includeExpired = false)
    {

    	if (empty($token))
	    {
	    	return null;
	    }

	    if ($includeExpired)
	    {
		    $tokenRecord = TokenAuth_UserTokenRecord::model()->findByAttributes([
			    'token' => $token
		    ]);
	    }
	    else
        {
	        $tokenRecord = TokenAuth_UserTokenRecord::model()->notExpired()->findByAttributes([
		        'token' => $token
	        ]);
	    }

	    if (!empty($tokenRecord))
	    {
		    return TokenAuth_UserTokenModel::populateModel($tokenRecord);
	    }
	    else
	    {
		    return null;
	    }

    }


	/**
	 * Attempts to authenticate a current user based on a provided token.
	 *
	 * @param TokenAuth_UserTokenModel|string $token
	 *
	 * @return TokenAuth_UserTokenModel|false
	 */
    public function authenticateToken($token = null)
    {

    	// If they haven't passed in a Token model explicitly, try to find one.

	    if (empty($token))
	    {
		    return false;
	    }
	    elseif (!($token instanceof TokenAuth_UserTokenModel))
	    {
		    $token = $this->getTokenByString($token);
	    }

	    // If we have a valid token in hand, try to authenticate it.

	    if ($token instanceof TokenAuth_UserTokenModel && $token->expiryDate > DateTimeHelper::currentUTCDateTime())
	    {

	    	if (craft()->userSession->loginByUserId($token->userId))
		    {
			    $this->setActiveToken($token);
			    return $token;
		    }
		    else
		    {
		    	return false;
		    }

	    }
	    else
	    {
	    	return false;
	    }

    }


	/**
	 * Attempts to authenticate a token contained in the params of the current request.
	 * The method will use the parameter named by the $param argument if it is given,
	 * or fall back to the parameter named in the config.
	 *
	 * @param string|null $param
	 *
	 * @return TokenAuth_UserTokenModel|false
	 */
    public function authenticateCurrentRequestToken($param = null)
    {

    	if (empty($param))
	    {
	    	$param = craft()->config->get('defaultTokenParam', 'tokenauth');
	    }

	    $currentToken = craft()->request->getParam($param);

	    return $this->authenticateToken($currentToken);

    }


	/**
	 * Attempts to extend the expiry date of the provided token.
	 * Optionally can extend the token even if it has expired.
	 *
	 * @param TokenAuth_UserTokenModel|string $token
	 * @param bool $extendIfExpired
	 *
	 * @return bool
	 */
    public function extendToken($token = null, $extendIfExpired = false)
    {

	    // Make sure we have a token

	    if (empty($token))
	    {
		    return false;
	    }
	    elseif ($token instanceof TokenAuth_UserTokenModel)
	    {
		    $token = $token->token;
	    }
	    elseif(!is_string($token))
	    {
		    return false;
	    }

	    // Try to find a token record

	    $tokenRecord = TokenAuth_UserTokenRecord::model()->findByAttributes([
		    'token' => $token
	    ]);

	    if(empty($tokenRecord))
	    {
	    	return false;
	    }

	    // Ok, we have a token. Try to extend it.

	    if ($tokenRecord->expiryDate > DateTimeHelper::currentUTCDateTime() || $extendIfExpired)
	    {

		    $expiryDate = DateTimeHelper::currentUTCDateTime();
		    $expiryDate->add(new DateInterval($this->_defaultTokenDuration));

		    if ($expiryDate > $token->expiryDate)
		    {
			    $tokenRecord->expiryDate = $expiryDate;
			    return $tokenRecord->save();
		    }
		    else
		    {
			    return true;
		    }

	    }

	    return false;

    }


	/**
	 * Attempts to expire the provided token, if it is not already expired.
	 *
	 * @param TokenAuth_UserTokenModel|string $token
	 *
	 * @return bool
	 */
	public function expireToken($token = null)
	{

		// Make sure we have a token

		if (empty($token))
		{
			return false;
		}
		elseif ($token instanceof TokenAuth_UserTokenModel)
		{
			$token = $token->token;
		}
		elseif(!is_string($token))
		{
			return false;
		}

		// Try to find a token record

		$tokenRecord = TokenAuth_UserTokenRecord::model()->findByAttributes([
			'token' => $token
		]);

		if(empty($tokenRecord))
		{
			return false;
		}

		// Ok, we have a token. Try to expire it.

		if ($tokenRecord->expiryDate > DateTimeHelper::currentUTCDateTime())
		{
			$tokenRecord->expiryDate = DateTimeHelper::currentUTCDateTime();
			return $tokenRecord->save();
		}
		else
		{
			return true;
		}

	}


	/**
	 * Returns the currently authenticated token, if there is one.
	 *
	 * @return TokenAuth_UserTokenModel|null
	 */
    public function getActiveToken()
    {
    	return $this->_activeToken;
    }


	/**
	 * Sets the currently authenticated token, if there is one.
	 *
	 * @param TokenAuth_UserTokenModel|null $token
	 */
	public function setActiveToken($token)
	{
		if ($token instanceof TokenAuth_UserTokenModel || is_null($token))
		{
			$this->_activeToken = $token;
		}
	}


}

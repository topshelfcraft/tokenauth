<?php
namespace Craft;

/**
 * TokenAuth_UserTokenModel
 *
 * @author    Top Shelf Craft <michael@michaelrog.com>
 * @copyright Copyright (c) 2016, Michael Rog
 * @license   http://topshelfcraft.com/license
 * @see       http://topshelfcraft.com
 * @package   craft.plugins.tokenauth
 * @since     1.0
 */
class TokenAuth_UserTokenModel extends BaseModel
{


	/**
	 * @return array
	 */
    public function defineAttributes()
    {

        return array(
            'userId' => AttributeType::Number,
            'token' => AttributeType::String,
            'expiryDate' => AttributeType::DateTime,
        );

    }


}

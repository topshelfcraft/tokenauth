<?php
namespace Craft;

/**
 * TokenAuth_UserTokenRecord
 *
 * @author    Top Shelf Craft <michael@michaelrog.com>
 * @copyright Copyright (c) 2016, Michael Rog
 * @license   http://topshelfcraft.com/license
 * @see       http://topshelfcraft.com
 * @package   craft.plugins.tokenauth
 * @since     1.0
 */
class TokenAuth_UserTokenRecord extends BaseRecord
{


	// Public Methods
	// =========================================================================


	/**
	 * @inheritDoc BaseRecord::getTableName()
	 *
	 * @return string
	 */
	public function getTableName()
	{
		return 'tokenauth_usertokens';
	}


	/**
	 * @inheritDoc BaseRecord::defineRelations()
	 *
	 * @return array
	 */
	public function defineRelations()
	{

		return array(
			'user' => array(static::BELONGS_TO, 'UserRecord', 'onDelete' => static::CASCADE),
		);

	}


	/**
	 * @inheritDoc BaseRecord::scopes()
	 *
	 * @return array
	 */
	public function scopes()
	{

		return array(
			'notExpired' => array(
				'condition' => 'expiryDate > NOW()',
			),
			'expired' => array(
				'condition' => 'expiryDate <= NOW()',
			),
			'longestValid'=>array(
				'order' => 'expiryDate DESC',
				'limit' => 1,
			),
		);

	}


	/**
	 * @return array
	 */
	public function defineIndexes()
	{

		return array(
			[ 'columns' => ['token'], 'unique' => true ],
		);

	}


	// Protected Methods
	// =========================================================================


	/**
	 * @inheritDoc BaseRecord::defineAttributes()
	 *
	 * @return array
	 */
	protected function defineAttributes()
	{

		return array(
			'token' => array(AttributeType::String, 'required' => true),
			'expiryDate' => AttributeType::DateTime,
		);

	}


}

<?php

/**
 * TokenAuth Plugin Config
 *
 * @author    Top Shelf Craft <michael@michaelrog.com>
 * @copyright Copyright (c) 2016, Michael Rog
 * @license   http://topshelfcraft.com/license
 * @see       http://topshelfcraft.com
 * @package   craft.plugins.tokenauth
 * @since     1.0
 */

return [

	'defaultTokenDuration' => null,
	'defaultTokenParam' => 'user_token',
	'userCanExtendOwnToken' => true,
	'userCanExpireOwnToken' => true,

];

<?php
namespace Craft;

/**
 * TokenAuthService
 *
 * @author    Top Shelf Craft <michael@michaelrog.com>
 * @copyright Copyright (c) 2016, Michael Rog
 * @license   http://topshelfcraft.com/license
 * @see       http://topshelfcraft.com
 * @package   craft.plugins.tokenauth
 * @since     1.0
 */
class TokenAuthVariable
{

	/**
	 * @return TokenAuth_UserTokenModel|null
	 */
    function getCurrentToken()
    {
        return craft()->tokenAuth->getActiveToken();
    }

};
